﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SearchableDropDown.Models;

namespace SearchableDropDown.Controllers
{
    public class HomeController : Controller
    {
        DropDownItems _dropDownItems;
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult LoadDropDownPage()
        {
           
            List<DropDownItems> dropDownItemList = new List<DropDownItems>();
            _dropDownItems = new DropDownItems
            {
                Value = "1",
                Name = "MobilePhones,Laptops and Gadgets"
            };
            dropDownItemList.Add(_dropDownItems);

            _dropDownItems = new DropDownItems
            {
                Value = "2",
                Name = "Pen,Pencil and Rubber"
            };
            dropDownItemList.Add(_dropDownItems);

            _dropDownItems = new DropDownItems();
            _dropDownItems.Value = "3";
            _dropDownItems.Name = "Bags,Suitcase and sports Items";
            dropDownItemList.Add(_dropDownItems);

            _dropDownItems = new DropDownItems
            {
                Value = "4",
                Name = "zzz,xxx,yyy,ooo"
            };
            dropDownItemList.Add(_dropDownItems);

            int valueToSelect=3;
            SelectList dropList;

            if (TempData["SelectedItemValue"] != null)
            {
                var selectedItemValue = TempData["SelectedItemValue"];
                 dropList = new SelectList(dropDownItemList, "Value", "Name", selectedItemValue);               
                var selectedText = dropDownItemList.Single(x => x.Value == selectedItemValue.ToString()).Name;
                ViewBag.ItemSelected = selectedText;
            }

            else
            {
                 dropList = new SelectList(dropDownItemList, "Value", "Name", dropDownItemList.Single(x => x.Value == valueToSelect.ToString()).Value);
                ViewBag.ItemSelected = dropDownItemList.Single(x => x.Value == valueToSelect.ToString()).Name;
            }
           
            ViewData["DropListItems"] = dropList;
            return View();
        }

        [HttpPost]
        public ActionResult GetDropDownSelectedItem(DropDownItems model)
        {
            TempData["SelectedItemValue"] = model.Value;
            return RedirectToAction("LoadDropDownPage");
        }


    }
   

}